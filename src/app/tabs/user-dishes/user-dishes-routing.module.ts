import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserDishesPage } from './user-dishes.page';

const routes: Routes = [
  {
    path: '',
    component: UserDishesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserDishesPageRoutingModule {}
