import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserDishesPage } from './user-dishes.page';

describe('UserDishesPage', () => {
  let component: UserDishesPage;
  let fixture: ComponentFixture<UserDishesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDishesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserDishesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
