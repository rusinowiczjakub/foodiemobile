import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserDishesPageRoutingModule } from './user-dishes-routing.module';

import { UserDishesPage } from './user-dishes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserDishesPageRoutingModule
  ],
  declarations: [UserDishesPage]
})
export class UserDishesPageModule {}
