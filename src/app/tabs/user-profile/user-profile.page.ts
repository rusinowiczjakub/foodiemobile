import {Component, OnInit} from '@angular/core';
import {NativeTabAnimationMixin} from '../NativeTabAnimationMixin';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.page.html',
    styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage extends NativeTabAnimationMixin implements OnInit {

    constructor() {
        super();
    }

    ngOnInit() {
    }

}
