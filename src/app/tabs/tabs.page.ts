import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSlides, NavController} from '@ionic/angular';
import {NativePageTransitions, NativeTransitionOptions} from '@ionic-native/native-page-transitions/ngx';


@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.page.html',
    styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
    @ViewChild('tabs') tabs: HTMLIonTabsElement;
    @ViewChild(IonSlides) slides: IonSlides;

    loaded: boolean = false;
    tabIndex: number = 0;
    tabsIndexes = {
        'explore': 0,
        'dishes': 1,
        'shopping-list': 2,
        'user-profile': 3
    };

    ionViewWillEnter() {
        this.tabIndex = 0;
    }

    constructor(public navCtrl: NavController, private nativePageTransitions: NativePageTransitions) {
    }

    ngOnInit() {
    }

    private getAnimationDirection(tab: string): string {
        const currentIndex = this.tabIndex;
        this.tabIndex = this.tabsIndexes[tab];

        switch (true) {
            case (currentIndex < this.tabIndex):
                return ('left');
            case (currentIndex > this.tabIndex):
                return ('right');
        }
    }

    public transition(e: any): void {
        console.log(e);
        const options: NativeTransitionOptions = {
            direction: this.getAnimationDirection(e.tab),
            duration: 250,
            // slowdownfactor: -1,
            slidePixels: 0,
            iosdelay: 20,
            androiddelay: 0,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };

        if (!this.loaded) {
            this.loaded = true;
            return;
        }

        this.nativePageTransitions.slide(options);
    }
}
