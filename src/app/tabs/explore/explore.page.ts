import {Component, OnInit} from '@angular/core';
import {NativeTabAnimationMixin} from '../NativeTabAnimationMixin';

@Component({
    selector: 'app-explore',
    templateUrl: './explore.page.html',
    styleUrls: ['./explore.page.scss'],
})
export class ExplorePage extends NativeTabAnimationMixin implements OnInit {

    categories: any[] = [
        {
            id: 1,
            name: 'Pizza',
            image: 'https://www.mojegotowanie.pl/uploads/media/recipe/0001/100/pizza-z-kurczakiem.jpeg'
        },
        {
            id: 2,
            name: 'Steak',
            image: 'https://i2.wp.com/www.foodrepublic.com/wp-content/uploads/2012/05/testkitchen_argentinesteak.jpg?resize=1280%2C%20560&ssl=1'
        },
        {
            id: 3,
            name: 'Burgers',
            image: 'https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/vimdb/72589_533-0-4473-4473.jpg'
        },
        {
            id: 4,
            name: 'Kethogenic',
            image: 'https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/vimdb/72589_533-0-4473-4473.jpg'
        },
        {
            id: 5,
            name: 'Vegetables',
            image: 'https://cdn.pixabay.com/photo/2016/12/26/17/28/food-1932466__340.jpg'
        },
        {
            id: 6,
            name: 'Desserts',
            image: 'https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/vimdb/72589_533-0-4473-4473.jpg'
        },
        {
            id: 7,
            name: 'Pasta',
            image: 'https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/vimdb/72589_533-0-4473-4473.jpg'
        },
        {
            id: 8,
            name: 'Soup',
            image: 'https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/vimdb/72589_533-0-4473-4473.jpg'
        },
        {
            id: 8,
            name: 'Mexican',
            image: 'https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/vimdb/72589_533-0-4473-4473.jpg'
        },
    ];

    constructor() {
        super();
    }

    ngOnInit() {
    }
}
