export class NativeTabAnimationMixin {
    protected entered = false;

    protected ionViewWillEnter() {
        setTimeout(() => {
            this.entered = true;
        }, 50);
    }

    protected ionViewWillLeave() {
        this.entered = false;
    }
}
