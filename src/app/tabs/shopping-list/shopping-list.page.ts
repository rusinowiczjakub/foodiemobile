import {Component, OnInit} from '@angular/core';
import {NativeTabAnimationMixin} from '../NativeTabAnimationMixin';

@Component({
    selector: 'app-shopping-list',
    templateUrl: './shopping-list.page.html',
    styleUrls: ['./shopping-list.page.scss'],
})
export class ShoppingListPage extends NativeTabAnimationMixin implements OnInit {

    constructor() {
        super();
    }

    ngOnInit() {
    }

}
