import {Component, OnInit} from '@angular/core';
import {NativeTabAnimationMixin} from '../NativeTabAnimationMixin';

@Component({
    selector: 'app-dishes',
    templateUrl: './dishes.page.html',
    styleUrls: ['./dishes.page.scss'],
})
export class DishesPage extends NativeTabAnimationMixin implements OnInit {
    categories: any[] = [
        {
            id: 0,
            name: 'All'
        },
        {
            id: 1,
            name: 'Pizza'
        },
        {
            id: 2,
            name: 'Steak'
        },
        {
            id: 3,
            name: 'Burgers'
        },
        {
            id: 4,
            name: 'Kethogenic'
        },
        {
            id: 5,
            name: 'Vegetables'
        },
        {
            id: 6,
            name: 'Desserts'
        },
        {
            id: 7,
            name: 'Pasta'
        },
        {
            id: 8,
            name: 'Soup'
        },
        {
            id: 9,
            name: 'Mexican',
        },
    ];

    activeCategoryId = 0;

    constructor() {
        super();
    }

    ngOnInit() {
    }
}
