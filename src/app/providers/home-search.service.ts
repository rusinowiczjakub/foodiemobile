import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HomeSearchService {

    private readonly _searchActive = new BehaviorSubject(false);
    readonly searchActive$ = this._searchActive.asObservable();

    private readonly _lastSearches = new BehaviorSubject(['Pulled pork', 'Cheesburger', 'Steak']);
    readonly lastSearches$ = this._lastSearches.asObservable();

    constructor() {
    }

    set state(state) {
        this._searchActive.next(state);
    }

    get state() {
        return this._searchActive.getValue();
    }
}
