import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.scss'],
})
export class NewRecipeComponent implements OnInit {


  @Input() image: string;
  @Input() name: string;
  @Input() prepTime: number;
  @Input() steps: number;
  @Input() category: string;

  constructor() { }

  ngOnInit() {}

}
