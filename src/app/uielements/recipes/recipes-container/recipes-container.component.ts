import {Component, ContentChildren, OnInit, QueryList} from '@angular/core';
import {CategoryCardComponent} from '../../category/category-card/category-card.component';
import {PopularRecipeComponent} from '../popular-recipe/popular-recipe.component';
import {NewRecipeComponent} from '../new-recipe/new-recipe.component';

@Component({
  selector: 'app-recipes-container',
  templateUrl: './recipes-container.component.html',
  styleUrls: ['./recipes-container.component.scss'],
})
export class RecipesContainerComponent implements OnInit {

  @ContentChildren(PopularRecipeComponent) popularRecipes: QueryList<PopularRecipeComponent>;
  @ContentChildren(NewRecipeComponent) newRecipes: QueryList<NewRecipeComponent>;

  constructor() { }

  ngOnInit() {}

}
