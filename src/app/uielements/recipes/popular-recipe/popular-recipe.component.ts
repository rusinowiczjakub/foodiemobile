import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-popular-recipe',
    templateUrl: './popular-recipe.component.html',
    styleUrls: ['./popular-recipe.component.scss'],
})
export class PopularRecipeComponent implements OnInit {

    @Input() image: string;
    @Input() name: string;
    @Input() prepTime: number;
    @Input() steps: number;

    constructor() {
    }

    ngOnInit() {
    }

}
