import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {ModalController} from '@ionic/angular';
import {SearchModalComponent} from './search-modal/search-modal.component';
import {HomeSearchService} from '../../providers/home-search.service';

@Component({
    selector: 'app-search-input',
    templateUrl: './search-input.component.html',
    styleUrls: ['./search-input.component.scss'],
    animations: [
        trigger(
            'inAnimationBack',
            [
                transition(
                    ':enter',
                    [
                        style({width: 0, opacity: 0}),
                        animate('0.5s ease-out',
                            style({width: 40, opacity: 1}))
                    ]
                ),
                transition(
                    ':leave',
                    [
                        style({width: 40, opacity: 1}),
                        animate('0.3s ease-in',
                            style({width: 0, opacity: 0}))
                    ]
                )
            ]
        )
    ]
    // encapsulation: ViewEncapsulation.None
})
export class SearchInputComponent implements OnInit {

    focused;
    searchModal;

    constructor(
        public modalCtrl: ModalController,
        private provider: HomeSearchService
    ) {

    }

    ngOnInit() {
        // this.searchModal = await this.modalCtrl.create({
        //     component: SearchModalComponent,
        //     cssClass: 'modal-not-full',
        //     swipeToClose: true,
        //     showBackdrop: false
        // });
    }

    openModal() {
        this.focused = true;
        this.provider.state = true;
    }

    closeModal() {
        this.focused = false;
        this.provider.state = false;
    }
}
