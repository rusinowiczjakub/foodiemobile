import {Component, OnDestroy, OnInit} from '@angular/core';
import {HomeSearchService} from '../../../providers/home-search.service';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'app-search-modal',
    templateUrl: './search-modal.component.html',
    styleUrls: ['./search-modal.component.scss'],
    animations: [
        trigger(
            'inAnimation',
            [
                transition(
                    ':enter',
                    [
                        style({height: 0, opacity: 0}),
                        animate('0.5s ease-out',
                            style({height: '100%', opacity: 1}))
                    ]
                ),
                transition(
                    ':leave',
                    [
                        style({height: '100%', opacity: 1}),
                        animate('0.5s ease-in',
                            style({height: 0, opacity: 0}))
                    ]
                )
            ]
        )
    ]
})
export class SearchModalComponent implements OnInit, OnDestroy {

    lastSearches = [];

    constructor(
        private provider: HomeSearchService
    ) {
    }

    ngOnDestroy(): void {
    }


    ngOnInit() {
        const tabs = document.querySelectorAll('ion-tab-bar');
        this.provider
            .searchActive$
            .subscribe((state) => {
                switch (state) {
                    case true:
                        if (tabs !== null) {
                            Object.keys(tabs).map((key) => {
                                tabs[key].style.display = 'none';
                            });
                        }

                        break;

                    case false:
                        if (tabs !== null) {
                            Object.keys(tabs).map((key) => {
                                tabs[key].style.display = 'flex';
                            });
                        }

                }
            });
    }

}
