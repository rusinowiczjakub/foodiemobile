import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
    selector: '[appScrollSpeed]'
})
export class ScrollSpeedDirective {

    startPosX = 0;
    startPosY = 0;
    endPosX = 0;
    endPosY = 0;

    // @Input() target;

    constructor(el: ElementRef) {
    }

    // @HostListener('scroll', ['$event'])
    // onDrag(event: Event) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //     // event.target.scrollLeft = event.target.scrollLeft / 2;
    //     // event.preventDefault();
    // }
    //
    // @HostListener('touchstart', ['$event'])
    // touchStart(event: Event) {
    //     console.log(event);
    //     this.startPosX = event.touches[0].pageX;
    //     this.startPosY = event.touches[0].pageY;
    //     // console.log(event);
    // }
    //
    // @HostListener('touchend', ['$event'])
    // touchEnd(event: Event) {
    //     this.endPosX = event.changedTouches[0].pageX;
    //     this.endPosY = event.changedTouches[0].pageY;
    //     event.target.scrollLeft += this.startPosX - this.endPosX;
    //     console.log(this.startPosX - this.endPosX);
    // }
}
