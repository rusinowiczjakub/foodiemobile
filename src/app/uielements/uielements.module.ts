import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoriesContainerComponent} from './category/categories-container/categories-container.component';
import {CategoryCardComponent} from './category/category-card/category-card.component';
import {SearchInputComponent} from './search-input/search-input.component';
import {IonicModule} from '@ionic/angular';
import {SearchModalComponent} from './search-input/search-modal/search-modal.component';
import {RecipesContainerComponent} from './recipes/recipes-container/recipes-container.component';
import {NewRecipeComponent} from './recipes/new-recipe/new-recipe.component';
import {PopularRecipeComponent} from './recipes/popular-recipe/popular-recipe.component';
import { ScrollSpeedDirective } from './scroll-speed.directive';
import {CategoryTextComponent} from './category/category-text/category-text.component';
import { HideOnChangeDirective } from './hide-on-change.directive';


@NgModule({
    declarations: [
        CategoriesContainerComponent,
        CategoryCardComponent,
        SearchInputComponent,
        SearchModalComponent,
        RecipesContainerComponent,
        NewRecipeComponent,
        PopularRecipeComponent,
        ScrollSpeedDirective,
        CategoryTextComponent,
        HideOnChangeDirective
    ],
    imports: [
        CommonModule,
        IonicModule
    ],
    exports: [
        CategoryCardComponent,
        CategoriesContainerComponent,
        SearchInputComponent,
        SearchModalComponent,
        RecipesContainerComponent,
        NewRecipeComponent,
        PopularRecipeComponent,
        ScrollSpeedDirective,
        CategoryTextComponent,
        HideOnChangeDirective
    ],
    entryComponents: [
        SearchModalComponent
    ]
})
export class UIElementsModule {
}
