import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-category-text',
  templateUrl: './category-text.component.html',
  styleUrls: ['./category-text.component.scss'],
})
export class CategoryTextComponent implements OnInit {

  @Input() name;
  @Input() active = false;

  constructor() { }

  ngOnInit() {}

}
