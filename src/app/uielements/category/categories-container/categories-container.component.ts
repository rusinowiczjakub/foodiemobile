import {AfterContentInit, Component, ContentChildren, OnInit, QueryList} from '@angular/core';
import {CategoryCardComponent} from '../category-card/category-card.component';

@Component({
    selector: 'app-categories-container',
    templateUrl: './categories-container.component.html',
    styleUrls: ['./categories-container.component.scss'],
})
export class CategoriesContainerComponent implements OnInit, AfterContentInit {

    @ContentChildren(CategoryCardComponent) categories: QueryList<CategoryCardComponent>;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
    }
}
